//
//  AppDelegate.h
//  CustomUIImageControl
//
//  Created by Jaime Moises Gutierrez on 10/21/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

