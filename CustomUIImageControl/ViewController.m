//
//  ViewController.m
//  CustomUIImageControl
//
//  Created by Jaime Moises Gutierrez on 10/21/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UISlider *scalingSlider;
@property (strong, nonatomic) UIBezierPath *bezierPath;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedController;
@property (strong, nonatomic) UIImage *image;

@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageView.contentMode = UIViewContentModeScaleToFill;

    self.image = [UIImage imageNamed:@"cat"];
    self.imageView.image = self.image;
    
// setup slider
    self.scalingSlider.minimumValue = 0.1;
    self.scalingSlider.maximumValue = 1.0;
    self.scalingSlider.value = 1.0;
    
    [self setButtonsOriginalCenter];
    
    [self updateBezierPaths];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)resetView:(id)sender
{
    [self unmaskUIImage:self.imageView];

    [self viewDidLoad];
}

#pragma mark - Image Manipulations

- (UIImage *)cropImage:(UIImage *)image toRect:(CGRect)rect
{
    CGImageRef imref = CGImageCreateWithImageInRect([image CGImage], rect);
    return [UIImage imageWithCGImage:imref];
}

#pragma mark - slider methods

- (IBAction)sliderChangedValue:(id)sender
{
    UISlider *slider = sender;
    
    self.image = [UIImage imageNamed:@"cat"];
    
    self.image = [self cropImage:self.image toRect:CGRectMake(0, 0,self.imageView.frame.size.width*slider.value, self.imageView.frame.size.width*slider.value)];
    
    self.imageView.image = self.image;
    [self maskImage];
}

#pragma mark - masking

- (void)maskImage
{
    if (self.segmentedController.selectedSegmentIndex == 1)
    {
        CAShapeLayer *mask = [CAShapeLayer layer];
        mask.path = self.bezierPath.CGPath;
        self.imageView.layer.mask = mask;
    }
    [self updateBezierPaths];
}

- (void)unmaskUIImage:(UIImageView *)imageView
{
    imageView.layer.mask = nil;
}

#pragma mark - segmented control actions

- (IBAction)segmentedControlValueChanged:(id)sender
{
    UISegmentedControl *sc = sender;
    if (sc.selectedSegmentIndex == 0)
    {
        [self unmaskUIImage:self.imageView];
    }
    else
    {
        [self maskImage];
    }
}

#pragma mark - Bezier Paths

- (void)updateBezierPaths
{
    if (self.bezierPath == nil)
    {
        self.bezierPath = [UIBezierPath bezierPath];
    }
    
    [self.bezierPath removeAllPoints];
    
    // Set the starting point of the shape.

    [self.bezierPath moveToPoint:[self getRelativePointOfButton:self.button1]];
    [self.bezierPath addLineToPoint:[self getRelativePointOfButton:self.button2]];
    [self.bezierPath addLineToPoint:[self getRelativePointOfButton:self.button3]];
    [self.bezierPath addLineToPoint:[self getRelativePointOfButton:self.button4]];
    [self.bezierPath closePath];
    
    [self drawBezierPaths];
}

- (void)drawBezierPaths
{
    self.imageView.image = self.image;
    
    UIGraphicsBeginImageContext(self.imageView.frame.size);
    
    [self.image drawInRect:CGRectMake(self.imageView.frame.origin.x - self.imageView.frame.origin.x, self.imageView.frame.origin.y - self.imageView.frame.origin.y, self.imageView.frame.size.width, self.imageView.frame.size.height)];
    
    self.bezierPath.lineWidth = 2;
    [[UIColor blackColor] setStroke];
    [self.bezierPath stroke];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddPath(context,self.bezierPath.CGPath);
    self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
}

#pragma mark - Button Setup

- (void)setButtonsOriginalCenter
{
    self.button1.center = self.imageView.frame.origin;
    self.button2.center = CGPointMake(self.imageView.frame.origin.x+self.imageView.frame.size.width,self.imageView.frame.origin.y);
    self.button3.center = CGPointMake(self.imageView.frame.origin.x+self.imageView.frame.size.width,self.imageView.frame.origin.y+self.imageView.frame.size.height);
    self.button4.center = CGPointMake(self.imageView.frame.origin.x,self.imageView.frame.origin.y+self.imageView.frame.size.height);
}

- (CGPoint)getRelativePointOfButton:(UIButton *)button
{
    return CGPointMake(button.center.x - self.imageView.frame.origin.x,button.center.y - self.imageView.frame.origin.y);
}

#pragma mark - Button Actions

- (IBAction)buttonPressed:(id)sender forEvent:(UIEvent*)event
{
    UIView *button = (UIView *)sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    CGPoint location = [touch locationInView:self.imageView];
    button.frame = CGRectMake(location.x + button.frame.size.width, location.y + button.frame.size.height, button.frame.size.width, button.frame.size.width);
    NSLog(@"Location in button: %f, %f", location.x, location.y);
    [self updateBezierPaths];
    [self maskImage];
}


@end